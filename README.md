# What is This?
Just an extra repo for my Arch Linux installation. Most of the packages here are the packages that not in [chaotic-aur](https://aur.chaotic.cx/).


# How To


## Install GPG Key
- [ ] Run this command in your terminal to add my public GPG key.
```
sudo pacman-key --recv-key 08CC97E3B4C095EC
```
- [ ] And then, run this to sign my public GPG key to pacman.
```
sudo pacman-key --lsign-key 08CC97E3B4C095EC
```

## Add Repo to Pacman
- [ ] Append this lines to your end /etc/pacman.conf.
```
[xtend]
SigLevel = Required DatabaseOptional
Server = https://gitlab.com/pnplusplus/archlinux-pnplusplus-repo/-/raw/main/$arch

```
